# ss2021-solid-abelian-groups

These are notes for a talk I gave about solid abelian groups in the research seminar of the ESAGA in the summer term 2021 (organized by Nicolas Dupré and Chirantan Chowdhury).
A compiled version can be found [here](https://makoba.gitlab.io/ss2021-solid-abelian-groups/solid-abelian-groups.pdf).