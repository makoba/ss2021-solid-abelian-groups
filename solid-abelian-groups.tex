\documentclass[a4paper, 12pt]{scrartcl}

\usepackage{general}
\usepackage{mystyle}

\title{Solid Abelian Groups 1}
\author{}
\date{}

\begin{document}
    
    \maketitle
    

    Our goal in this talk is to motivate and to define the notion of a solid abelian group.
    Solid abelian groups will form an abelian subcategory $\catsolid \subseteq \Cond(\catabs)$ that enjoys many nice properties.
    Showing this will require hard work and will mostly be done in the next talk.
    
    \section{The structure of the condensed abelian groups $\ZZ[\underline{S}]$}
    
    Recall that the forgetful functor $\Cond(\catabs) \to \Cond(\catsets)$ has a left adjoint that we denote by $X \mapsto \ZZ[X]$.
    Concretely, for some $X \in \Cond(\catsets)$, the condensed abelian group $\ZZ[X] \colon \catchaus^{\op} \to \catabs$ is given as the sheafification of the presheaf
    \[
    S \mapsto \ZZ[X(S)] = \bigoplus_{X(S)} \ZZ.
    \]
    We will now try to understand the condensed abelian groups $\ZZ[X]$ a bit better (at least when $X = \underline{S}$ for a profinite set $S$).
    We start with the following observations ($S$ is some compact Hausdorff space):
    \begin{itemize}
        \item
        We have $\ZZ[\underline{\ast}] \cong \underline{\ZZ}$.
        
        \item
        Consequently we obtain a natural map
        \[
        \underline{\round[\Big]{\bigoplus_{s \in S} \ZZ}^{\disc}} \cong \bigoplus_{s \in S} \underline{\ZZ} \to \ZZ[\underline{S}].
        \]
        Note that this map is an isomorphism if $S$ is a finite discrete set because the functor $X \mapsto \ZZ[X]$ commutes with colimits.
        
        \item
        We also have a canonical map of topological spaces $S \to \round[\Big]{\bigoplus_{s \in S} \ZZ}^{\indisc}$ that induces a map of condensed abelian groups
        \[
        \ZZ[\underline{S}] \to \underline{\round[\Big]{\bigoplus_{s \in S} \ZZ}^{\indisc}}
        \]
        (probably the right hand side is not really a condensed abelian group but only a sheaf on $\catchaus$ but this causes no problems).
        
        \item
        The composition
        \[
        \underline{\round[\Big]{\bigoplus_{s \in S} \ZZ}^{\disc}} \to \ZZ[S] \to \underline{\round[\Big]{\bigoplus_{s \in S} \ZZ}^{\indisc}}
        \]
        is the obvious inclusion.
        In particular the first map is injective.
    \end{itemize}
    
    \begin{lemma}
        Let $S$ be a compact Hausdorff space.
        Then the map
        \[
        \ZZ[\underline{S}] \to \underline{\round[\Big]{\bigoplus_{s \in S} \ZZ}^{\indisc}}
        \]
        is injective.
        
        \begin{proof}
            Let $T \in \catchaus$ and let $f \in \ZZ[C(T, S)]$ be an element such that $f(t) = 0 \in \bigoplus_{s \in S} \ZZ$ for all $t \in T$.
            Then we have to show that $f$ also maps to zero under $\ZZ[C(T, S)] \to \ZZ[\underline{S}](T)$.
            Write $f = \sum_{j = 1}^k n_j \cdot [g_j]$ for distinct $g_j \in C(T, S)$ and nonzero $n_j \in \ZZ$ and argue by induction on $k$ (the base case $k = 0$ being trivial):
            For $1 \leq j < j' \leq k$ let
            \[
            T_{j, j'} \coloneqq \set[\big]{t \in T}{g_j(t) = g_{j'}(t)} \subseteq T.
            \]
            Then the $T_{j, j'}$ are closed subset of $T$.
            They also cover $T$ because if $t \in T$ would be an element not contained in any of the $T_{j, j'}$ then the $g_j(t) \in S$ would all be distinct and consequently the element
            \[
            f(t) = \sum_{j = 1}^k n_j \cdot [g_j(t)] \in \bigoplus_{s \in S} \ZZ
            \]
            would be nonzero.
            Thus it suffices to show that $f$ maps to zero in $\ZZ[\underline{S}](T_{j, j'})$ for all $j, j'$.
            But $f|_{T_{j, j'}}$ can be written as a \enquote{sum of length $k-1$} so that we win by our induction hypothesis (strictly speaking we should be careful and quantize the induction claim over $T$).
        \end{proof}
    \end{lemma}
    
    The takeaway from this is that we have sandwiched our $\ZZ[\underline{S}]$ between two condensed abelian groups that we (maybe) understand slightly better.
    However we are still far away from \enquote{really understanding} $\ZZ[\underline{S}]$.
    We now make the following definition (in the end it will turn out to only be well-behaved when $S$ is a profinite set):
    
    \begin{definition}
        Let $S \in \catchaus$.
        Then we define the condensed abelian group
        \[
        \ZZ[\underline{S}]^{\solid} \coloneqq \intHom(\intHom(\ZZ[\underline{S}], \underline{\ZZ}), \underline{\ZZ}).
        \]
        This defines a functor from the full subcategory of $\Cond(\catabs)$ spanned by the objects $\ZZ[\underline{S}]$ to $\Cond(\catabs)$.
        
        Note that there is a natural (evaluation) map of condensed abelian groups $\ZZ[\underline{S}] \to \ZZ[\underline{S}]^{\solid}$.
    \end{definition}
    
    \begin{lemma}
        Let $S \in \catchaus$.
        Then we have a natural isomorphism $\intHom(\ZZ[\underline{S}], \underline{\ZZ}) \cong \underline{C(S, \ZZ)^{\disc}}$.
        
        \begin{proof}
            Let $T \in \catchaus$.
            Then we have
            \begin{align*}
                \intHom(\ZZ[\underline{S}], \underline{\ZZ})(T) &\cong \Hom(\ZZ[\underline{S}] \otimes \ZZ[\underline{T}], \underline{\ZZ}) \\
                &\cong \Hom(\ZZ[\underline{S \times T}], \underline{\ZZ}) \\
                &\cong C(S \times T, \ZZ) \\
                &\cong C(T, C(S, \ZZ)^{\disc}) = \underline{C(S, \ZZ)^{\disc}}(T).
            \end{align*}
            Note that the compact-open topology on $C(S, \ZZ)$ coincides with the discrete topology.
        \end{proof}
    \end{lemma}
    
    From this we see that
    \[
    \ZZ[\underline{S}]^{\solid} \cong \intHom(\underline{C(S, \ZZ)^{\disc}}, \underline{\ZZ}) \cong \underline{\Hom(C(S, \ZZ), \ZZ)^{\co}}
    \]
    (where $\co$ stands for the compact-open topology).
    In particular we have $\ZZ[\underline{S}]^{\solid}(\ast) \cong \Hom(C(S, \ZZ), \ZZ)$.
    Elements in this abelian group can be thought of as integer-valued measures on the space $S$ (we can integrate an integer valued function on $S$ with respect to such a measure and obtain an integer).
    
    \begin{lemma}
        Let $S \in \Pro(\catfinsets)$.
        Then the map $\ZZ[\underline{S}] \to \ZZ[\underline{S}]^{\solid}$ is injective.
        
        \begin{proof}
            We have a commutative diagram
            \[
            \begin{tikzcd}
                \ZZ[\underline{S}] \arrow{r} \arrow{d}
                &\underline{\round[\Big]{\bigoplus_{s \in S} \ZZ}^{\indisc}} \arrow{d}
                \\
                \ZZ[\underline{S}]^{\solid} \arrow{r}
                &\underline{\Hom(C(S, \ZZ), \ZZ)^{\indisc}}
            \end{tikzcd}
            \]
            where the right vertical map is given by the evaluation map
            \[
            \bigoplus_{s \in S} \ZZ \to \Hom(C(S, \ZZ), \ZZ), [s] \mapsto \round[\Big]{f \mapsto f(s)}.
            \]
            Thus (using that we already know the injectivity of the top vertical map) it suffices to show that this map is injective.
            So let $x = \sum_s n_s \cdot [s] \in \bigoplus_{s \in S} \ZZ$ and assume that $n_{s_0} \neq 0$ for some $s_0 \in S$.
            As there are only finitely many $s \in S$ with $n_s \neq 0$ we find a clopen neighborhood $U$ of $s_0$ in $S$ not containing any other of these finitely many points.
            Then the image of $x$ in $\Hom(C(S, \ZZ), \ZZ)$ maps $1_U$ to $n_{s_0} \neq 0$.
        \end{proof}
    \end{lemma}
    
    Thus we see that $\ZZ[\underline{S}](\ast) \subseteq \ZZ[\underline{S}]^{\solid}(\ast)$ are precisely the (finite) linear combinations of Dirac measures.
    The above lemma clearly fails for general compact Hausdorff spaces.
    For example if $S \in \catchaus$ is connected, then we have $C(S, \ZZ) \cong \ZZ$ and consequently $\ZZ[\underline{S}]^{\solid} \cong \underline{\ZZ}$.
    
    Now note that we for a finite set $S$ the natural map $\ZZ[\underline{S}] \to \ZZ[\underline{S}]^{\solid}$ is an isomorphism.
    In fact we have the following:
    
    \begin{lemma}
        The functor
        \[
        \catchaus \to \Cond(\catabs), \qquad S \mapsto \ZZ[\underline{S}]^{\solid}
        \]
        commutes with cofiltered limits.
        Consequently its restriction to $\Pro(\catfinsets)$ can be characterized as the unique cofiltered-limit-preserving extension of the functor
        \[
        \catfinsets \to \Cond(\catabs), \qquad S \mapsto \ZZ[\underline{S}].
        \]
        
        \begin{proof}
            Let $(S_i)_i$ be a cofiltered system in the category $\catchaus$ and let $S \coloneqq \varprojlim_i S_i$.
            Then we have $C(S, \ZZ) = \varinjlim_i C(S_i, \ZZ)$ (using that the $S_i$ are compact and $\ZZ$ is discrete) and consequently
            \begin{align*}
            \intHom(\underline{C(S, \ZZ)^{\disc}}, \underline{\ZZ}) &\cong \intHom(\varinjlim_i \underline{C(S_i, \ZZ)^{\disc}}, \underline{\ZZ}) \\
            &\cong \varprojlim_i \intHom(\underline{C(S_i, \ZZ)^{\disc}}, \underline{\ZZ}). \qedhere
            \end{align*}
        \end{proof}
    \end{lemma}
    
    The key to getting a description for $\ZZ[\underline{S}]$ is the following definition.
    
    \begin{definition}
        Let $n \in \ZZ_{\geq 0}$.
        For $S \in \Pro(\catfinsets)$ we define
        \[
        \ZZ[\underline{S}]_{\leq n}(\ast) \coloneqq \set[\Bigg]{\varphi \colon C(S, \ZZ) \to \ZZ}{\text{$\absval{\varphi(f)} \leq n$ for all $f \in C(S, \ZZ)$ with $\absval{f} \leq 1$}} \subseteq \ZZ[\underline{S}]^{\solid}(\ast).
        \]
        We then define $\ZZ[\underline{S}]_{\leq n} \subseteq \ZZ[\underline{S}]^{\solid}$ to be the associated condensed subset (i.e.\ the not-nice-looking fiber product $\underline{\ZZ[\underline{S}]_{\leq n}(\ast)^{\indisc}} \times_{\underline{\ZZ[\underline{S}]^{\solid}(\ast)^{\indisc}}} \ZZ[\underline{S}]^{\solid}$).
    \end{definition}
    
    On first glance it may not be clear why this definition agrees with the one in Clausen-Scholze but it is equivalent (and I think a little more descriptive).
    This is by no means my own insight and I want to thank Johannes for explaining it to me!
    
    \begin{remark}
        Let $n \in \ZZ_{\geq 0}$ and let $S$ be a finite set.
        Then we have $\ZZ[\underline{S}] = \ZZ[\underline{S}]^{\solid}$ (i.e.\ every measure is a linear combination of Dirac measures) and $\ZZ[\underline{S}]_{\ast}$ has the description
        \[
        \ZZ[\underline{S}]_{\leq n}(\ast) = \set[\Bigg]{ \sum_s n_s \cdot [s]}{\sum_s \absval{n_s} \leq n} \subseteq \ZZ[\underline{S}](\ast).
        \]
    \end{remark}
    
    \begin{lemma}
        We have the following properties ($S$ always a profinite set):
        \begin{itemize}
            \item
            The condensed subsets $\ZZ[\underline{S}]_{\leq n} \subseteq \ZZ[\underline{S}]^{\solid}$ are functorial in $S$.
            
            \item
            The functor
            \[
            \Pro(\catfinsets) \to \Cond(\catsets), \qquad S \mapsto \ZZ[\underline{S}]_{\leq n}
            \]
            commutes with cofiltered limits (and consequently is uniquely determined by this property and its restriction to $\catfinsets$).
            
            \item
            The condensed sets $\ZZ[\underline{S}]_{\leq n}$ \enquote{are} profinite sets.
            
            \item
            The maps
            \[
            \underline{\curly{-1, 0, 1}^n \times S^n} \to \ZZ[\underline{S}], \qquad \round[\big]{(a_i)_i, (f_i)_i} \mapsto \sum_i a_i f_i
            \]
            are surjective onto $\ZZ[\underline{S}]_{\leq n}$.
        \end{itemize}
        \begin{proof}
            For the first part note that if $\alpha \colon S \to S'$ is a map of profinite sets and $f \colon S' \to \ZZ$ is a continuous map with $\absval{f} \leq 1$ then also $\absval{f \circ \alpha} \leq 1$.
            
            For the second part we have to show that if $S = \lim_i S_i$ is a cofiltered limit in the category $\Pro(\catfinsets)$ then some $\varphi \colon C(S, \ZZ) \to \ZZ$ is contained in $\ZZ[\underline{S}]_{\leq n}(\ast)$ if and only if all the induced $\varphi_i \colon C(S_i, \ZZ) \to \ZZ$ are contained in $\ZZ[\underline{S_i}]_{\leq n}(\ast)$.
            But this is clear.
            
            The third part now follows from the second part and the observation that $\ZZ[\underline{S}]_{\leq n}$ \enquote{is} a finite set if $S \in \catfinsets$.
            
            Finally the last part is clear for finite sets.
            Thus for general profinite sets $S$ it follows that the map $\curly{-1, 0, 1}^n \times S^n \to \ZZ[\underline{S}](\ast) \subseteq \ZZ[\underline{S}]^{\solid}(\ast)$ has image inside $\ZZ[\underline{S}]_{\leq n}(\ast)$.
            That is surjective follows from the finite case and the (non-trivial!) fact that cofiltered limits of finite non-empty sets are non-empty.
        \end{proof}
    \end{lemma}
    
    \begin{remark}
        Combining the explicit description of $\ZZ[\underline{S}]_{\leq n}$ for finite sets $S$ with the fact that the functor $S \mapsto \ZZ[\underline{S}]_{\leq n}$ commutes with cofiltered limits gives us an explicit description for $\ZZ[\underline{S}]_{\leq n}$ for general profinite sets $S$ with a presentation $S = \varprojlim_i S_i$ as cofiltered limit of finite sets.
    \end{remark}
    
    \begin{corollary}
        Let $S \in \Pro(\catfinsets)$.
        Then we have
        \[
        \ZZ[\underline{S}] = \bigcup_n \ZZ[\underline{S}]_{\leq n}
        \]
        as equality of condensed abelian subgroups of $\ZZ[\underline{S}]^{\solid}$.
        \begin{proof}
            Each $\ZZ[\underline{S}]_{\leq n}$ is stable under taking inverses and contains the zero element.
            Moreover the addition on $\ZZ[\underline{S}]^{\solid}$ clearly carries $\ZZ[\underline{S}]_{\leq n} \times \ZZ[\underline{S}]_{\leq n'}$ into $\ZZ[\underline{S}]_{\leq n + n'}$ and the image of $\underline{S}$ in $\ZZ[\underline{S}]^{\solid}$ is contained in $\ZZ[\underline{S}]_{\leq 1}$.
            This shows the inclusion \enquote{$\subseteq$}.
            
            For the inclusion \enquote{$\supseteq$} we have to show that each $\ZZ[\underline{S}]_{\leq n}$ is contained in $\ZZ[\underline{S}]$.
            But this follows from the last point of the previous Lemma.
        \end{proof}
    \end{corollary}    
    
    \begin{remark}
        The (sheaf-theoretic) union $\bigcup_n \ZZ[\underline{S}]_{\leq n}$ can actually be computed as the presheaf-theoretic union as any covering of some object in $\catchaus$ has a finite refinement.
        Maybe more surprisingly the union can also be computed as the colimit in the category of topological spaces.
        
        However note that it does in general not agree with the set theoretic union inside $\Hom(C(S, \ZZ), \ZZ)^{\co}$ (with its induced topology), it only sits inside of this.
    \end{remark}
    
    The upshot of the discussion is that $\ZZ[\underline{S}](\ast)$ can also be described as the set of measures on $S$ with bounded operator norm (which is equivalent to being linear combination of Dirac measures) and this even describes $\ZZ[\underline{S}]$ as a condensed abelian group!
    On the other hand $\ZZ[\underline{S}]^{\solid}$ is the \enquote{set} of all measures on $S$ and thus can be viewed as a completed version of $\ZZ[\underline{S}]$.
    
    Next we have the following theorem due to Nöbeling.
    
    \begin{theorem}
        Let $S \in \Pro(\catfinsets)$.
        Then the (abstract) abelian group $C(S, \ZZ)$ is free.
        \begin{proof}
            We only give the construction of a basis of $C(S, \ZZ)$.
            Pick an inclusion $S \subseteq \prod_{\lambda} \curly{0, 1}$ for some ordinal $\lambda$.
            For every $\mu < \lambda$ let $f_{\mu} \colon S \to \ZZ$ be the composition of the projection to the $\mu$-th factor with the inclusion $\curly{0, 1} \subseteq \ZZ$.
            Order the finite products $f_{\mu_1} \dotsm f_{\mu_r}$ for $r \geq 0$ and $\lambda > \mu_1 > \dotsb > \mu_r$ lexicographically and let $B$ be the set of such products that cannot be written as a linear combination of smaller such products.
            Then $B$ is a basis for $C(S, \ZZ)$.
        \end{proof}
    \end{theorem}
    
    \begin{corollary}
        Let $S \in \Pro(\catfinsets)$.
        Then there exists a set $I$ and an isomorphism
        \[
        \ZZ[\underline{S}]^{\solid} \cong \prod_I \underline{\ZZ} \cong \underline{\prod_I \ZZ}
        \]
        where the product $\prod_I \ZZ$ is equipped with the product topology.
    \end{corollary}
    
    The above corollary shows that the condensed abelian group $\ZZ[\underline{S}]^{\solid}$ has a much easier structure than the complicated $\ZZ[\underline{S}]$.
    
    Now we can finally give two examples to feed our intuition:
    
    \begin{example}
        Let $S = \ZZ_{>0} \cup \curly{\infty}$ be the one-point-compactification of the positive integers.
        \begin{itemize}
            \item
            A basis for $C(S, \ZZ)$ is given by the continuous maps $f_s \coloneqq 1_{s}$ for $s \in \ZZ_{>0}$ and $f_{\infty} \coloneqq 1_S$ (here $1_U$ denotes the characteristic function on $U$).
            Let $I \coloneqq \ZZ_{>0} \cup \curly{\infty}$ be the index set for this basis (don't confuse $I$ with $S$).
            
            \item
            We identify $\ZZ[\underline{S}]^{\solid} \cong \prod_I \underline{\ZZ}$.
            
            \item
            The canonical map $S \to \ZZ[\underline{S}]^{\solid}$ maps $s \in \ZZ_{>0}$ to $e_s + e_{\infty}$ and it maps $\infty$ to $e_{\infty}$.
            Here the elements $e_i \in \prod_I \ZZ$ for $i \in I$ denote the \enquote{basis elements} of the product (that are not a basis).
            
            \item
            Let us see how the profinite subsets $\ZZ[\underline{S}]_{\leq n}(\ast) \subseteq \prod_I \ZZ$ look like.
            The maps
            \[
            g_j \colon S \to \curly{1, \dotsc, j, \infty}, \qquad s \mapsto \begin{cases} s &1 \leq s \leq j \\ \infty &\text{else}\end{cases}
            \]
            for $j \in \ZZ_{\geq 0}$ exhibit $S$ as a cofiltered limit of finite sets.
            Using this we obtain
            \[
            \ZZ[\underline{S}]_{\leq n}(\ast) \cong \set[\Bigg]{(k_i)_i \in \prod_I \ZZ}{ \forall j \in \ZZ_{\geq 0} \colon \sum_{t = 1}^j \absval{k_t} + \absval[\Big]{k_{\infty} - \sum_{t = 1}^j k_t} \leq n} \subseteq \prod_I \ZZ.
            \]
            
            \item
            The condensed abelian group $\ZZ[\underline{S}]$ is then the union of these profinite sets over all $n \in \ZZ_{\geq 0}$.
            Its underlying set is just given by $\bigoplus_S \ZZ \cong \bigoplus_I \ZZ \subseteq \prod_I \ZZ$ but it has more structure than that.
            For example the map
            \[
            S \to \prod_I \ZZ, \qquad \ZZ_{>0} \ni s \mapsto \sum_{i = s + 1}^{2s} e_i, \; \infty \mapsto 0
            \]
            is continuous (and even looks bounded) and maps into $\bigoplus_I \ZZ$ but its image is not contained in any of the $\ZZ[\underline{S}]_{\leq n}(\ast)$ and so it does not define an element in $\ZZ[\underline{S}](S)$.
        \end{itemize}
    \end{example}
    
    One can do a similar calculation for the Cantor set $S = \prod_{\ZZ_{>0}} \curly{0, 1}$.
    Here the index set $I$ of a basis for $C(S, \ZZ)$ looks more complicated (although it is still countable!) and I think one cannot expect $\bigoplus_S \ZZ \cong \bigoplus_I \ZZ \subseteq \prod_I \ZZ$ anymore.
    
    \section{Solid abelian groups and solid complexes}
    
    \begin{definition}
        A condensed abelian group $A$ is called solid if for all $S \in \Pro(\catfinsets)$ the natural map
        \[
        \Hom(\ZZ[\underline{S}]^{\solid}, A) \to \Hom(\ZZ[\underline{S}], A) \cong A(S)
        \]
        is an isomorphism in $\catabs$.
        Similarly an object $C \in D(\Cond(\catabs))$ is called solid if for all $S \in \Pro(\catfinsets)$ the natural map
        \[
        R \Hom(\ZZ[\underline{S}]^{\solid}, C) \to R \Hom(\ZZ[\underline{S}], C) \cong R \Gamma(S, C)
        \]
        is an isomorphism in $D(\catabs)$.
        
        We denote by $\catsolid \subseteq \Cond(\catabs)$ the full subcategory spanned by the solid abelian groups.
    \end{definition}
    
    In the above definition we can replace profinite sets by extremally disconnected compact Hausdorff spaces.
    This follows from the following Lemma (and the fact that every profinite set has a hypercovering by extremally disconnected compact Hausdorff spaces).
    
    \begin{lemma}
        Let $S_{\bullet} \to S$ be a hypercovering of a profinite set $S$ by profinite sets $S_i$.
        Then the complex
        \[
        \dotsb \to \ZZ[\underline{S_1}]^{\solid} \to \ZZ[\underline{S_0}]^{\solid} \to \ZZ[\underline{S}]^{\solid} \to 0
        \]
        is exact.
        \begin{proof}
            First we know that the sequence
            \[
            \dotsb \to \ZZ[\underline{S_1}] \to \ZZ[\underline{S_0}] \to \ZZ[\underline{S}] \to 0
            \]
            is exact.
            As $S$ and all the $S_i$ are profinite, all the terms in this sequence are $\Hom(\blank, \underline{\ZZ})$-acyclic.
            Thus we obtain the exact sequence
            \[
            0 \to C(S, \ZZ) \to C(S_0, \ZZ) \to C(S_1, \ZZ) \to \dotsb.
            \]
            Now all these (abstract) abelian groups are free and thus their associated condensed abelian groups are $\intHom(\blank, \underline{\ZZ})$-acyclic.
            Thus we obtain the exact sequence
            \[
            \dotsb \to \intHom(\underline{C(S_1, \ZZ)^{\disc}}, \underline{\ZZ}) \to \intHom(\underline{C(S_0, \ZZ)^{\disc}}, \underline{\ZZ}) \to \intHom(\underline{C(S, \ZZ)^{\disc}}, \underline{\ZZ}) \to 0
            \]
            as desired.
        \end{proof}
    \end{lemma}
    
    \section{Abstract category theory stuff things}
    
    Let $\calA$ be an abelian category with all colimits.
    Let $\calA_0 \subseteq \calA$ be a full subcategory spanned by compact projective objects generating $\calA$.
    Also assume that we are given a functor $F \colon \calA_0 \to A$ and a nameless natural transformation $i \to F$ (where $i \colon \calA_0 \to \calA$ denotes the inclusion).
    
    Then we let $\calA_F \subseteq \calA$ be the full subcategory spanned by all $Y \in \calA$ such that for all $X \in \calA_0$ the natural map
    \[
    \Hom(F(X), Y) \to \Hom(X, Y)
    \]
    is an isomorphism in $\catabs$.
    Similarly we let $D_F(\calA) \subseteq D(\calA)$ be the full subcategory spanned by all $C \in D(\calA)$ such that for all $X \in \calA_0$ the natural map
    \[
    R \Hom(F(X), C) \to R \Hom(X, C)
    \]
    is an isomorphism in $D(\catabs)$.
    
    \begin{example}
        We are interested in applying the material of this section to the following scenario:
        \begin{itemize}
            \item
            $\calA \coloneqq \Cond(\catabs)$ is the (abelian) category of condensed abelian groups.
            
            \item
            $\calA_0$ is spanned by the condensed abelian groups $\ZZ[S]$ for $S \in \catextdisc$.
            
            \item
            The functor $F$ is given by $\ZZ[S] \mapsto \ZZ[S]^{\solid}$ and the natural transformation is the obvious one.
        \end{itemize}
        Then the category $\calA_F$ (resp.\ $D_F(\calA)$) is spanned precisely by the solid abelian groups (resp.\ the solid objects in $\Cond(\catabs)$).
    \end{example}
    
    We will now also assume the following key property.
    
    \begin{keyproperty}
        Any connective complex $C$ of objects in $\calA$ all of whose terms are (possibly infinite) direct sums of objects in the essential image of $F$ is contained in $D_F(\calA)$.
    \end{keyproperty}
    
    In the next talk the following Theorem will be proven.
    
    \begin{theorem}
        The key property holds true in the case described in the above example.
    \end{theorem}
    
    The reason for introducing all this notation is that it gives a categorical framework for the situation we are in when considering solid abelian groups.
    Moreover we will (as far as I know) also use this framework in other situations later in the seminar.
    The main technical result is now the following:
    
    \begin{lemma}\label{lem:technical}
        We have the following properties:
        \begin{itemize}
            \item
            $\calA_F \subseteq \calA$ is an abelian subcategory stable under all limits, colimits and extensions.
            The objects $F(X)$ for $X \in \calA_0$ are compact projective generators for $\calA_F$.
            The inclusion $\calA_F \subseteq \calA$ has a left adjoint $L \colon \calA \to \calA_F$ that is the unique (up to unique isomorphism) colimit-preserving extension of $F \colon \calA_0 \to \calA_F$.
            
            \item
            The functor $D(\calA_F) \to D(\calA)$ is fully faithful with essential image $D_F(\calA)$.
            A complex $C \in D(\calA)$ is contained in $D_F(\calA)$ if and only if all $H^i(C) \in \calA$ are contained in $\calA_F$.
            The inclusion $D(\calA_F) \to D(\calA)$ has a left adjoint which is the left derived functor of $L$.
        \end{itemize}
    \end{lemma}
    
    \begin{corollary}
        We have the following properties:
        
        \begin{itemize}
            \item
            $\catsolid \subseteq \Cond(\catabs)$ is an abelian subcategory stable under all limits, colimits and extensions.
            The objects $\prod_I \ZZ \in \catsolid$, where $I$ is any set, form a family of compact projective generators.
            The inclusion $\catsolid \subseteq \Cond(\catabs)$ admits a left adjoint
            \[
            M \mapsto M^{\solid} \colon \Cond(\catabs) \to \catsolid
            \]
            that is the unique colimit-preserving extension of $\ZZ[\underline{S}] \mapsto \ZZ[\underline{S}]^{\solid}$ (for $S$ profinite).
            
            \item
            The functor $D(\catsolid) \to D(\Cond(\catabs))$ is fully faithful with essential image the solid objects in $D(\Cond(\catabs))$.
            An object $C \in D(\Cond(\catabs))$ is solid if and only if all $H^i(C) \in \Cond(\catabs)$ are solid.
            The inclusion $D(\catsolid) \to D(\Cond(\catabs))$ has a left adjoint
            \[
            C \mapsto C^{L\solid}
            \]
            which is the left derived functor of $M \mapsto M^{\solid}$.
        \end{itemize}
    \end{corollary}
    
    \subsection{Proof of (part of) \Cref{lem:technical}}
    
    We will now turn to the proof of \Cref{lem:technical}.
    We will not attempt to prove the whole Lemma but only aim for the first part of it.
    
    We start with the following observations:
    \begin{itemize}
        \item
        $\calA_F \subseteq \calA$ is stable under all limits (that exist in $\calA$).
        
        \item
        Direct sums of objects in the essential image of $F$ are contained in $\calA_F$ (use the key property).
        
        \item
        Maps from direct sums of objects in the essential image of $F$ lift along surjections between objects in $\calA_F$.
        
        \item
        Every object in $\calA_F$ admits a surjection from a direct sum of objects in the essential image of $F$.
    \end{itemize}
    
    The central argument for the proof of the first half of \Cref{lem:technical} is the content of the next two claims.
    
    \begin{claim}
        Let $Y, Z \in \calA$ be direct sums of objects in the essential image of $F$ and let $f \colon Y \to Z$ be a map with kernel $K$.
        Then $K[0] \in D_F(\calA)$ (and in particular $K \in \calA_F$).
        \begin{proof}
            We proceed as follows:
            \begin{itemize}
                \item
                Choose a quasi-isomorphism $B \to K[0]$ where $B$ is a connective complex such that for all $i \in \ZZ_{\geq 0}$ we have $B_i = \bigoplus_{j \in J_i} X_{ij}$ for some $X_{ij} \in \calA_0$ ($j \in J_i$).
                
                \item
                For $i \in \ZZ_{\geq 0}$ define $C_i \coloneqq \bigoplus_{j \in J_i} F(X_{ij})$.
                Using that the $X_{ij}$ are compact we see that the $C_i$ naturally form a connective complex $C$.
                We have a natural map $B \to C$.
                
                \item
                By the key property we know that $Y, Z \in \calA_F$.
                Thus also $K \in \calA_F$ and we see that the map $B \to K[0]$ uniquely extends to a map $C \to K[0]$.
                
                \item
                Now note that the map $B \to K[0]$ is a quasi-isomorphism so that we have shown that the map $B \to C$ is a split monomorphism in $D(\calA)$.
                Completing $B \to C$ to a distinguished triangle
                \[
                B \to C \to E \to B[1]
                \]
                we thus obtain $C \cong B \oplus E$ in $D(\calA)$.
                
                \item
                By the key property we have $C \in D_F(\calA)$ and this now implies $K \cong B \in D_F(\calA)$ because it is a direct summand of $C$. \qedhere
            \end{itemize}
        \end{proof}
    \end{claim}
    
    \begin{claim}
        Let $W \in \calA$.
        Then the following conditions are equivalent:
        \begin{itemize}
            \item
            $W[0] \in D_F(\calA)$.
            
            \item
            $W \in \calA_F$.
            
            \item
            $W$ is a cokernel of a map between two direct sums of objects in the essential image of $F$.
        \end{itemize}
        
        \begin{proof}
            We show the different implications.
            \begin{itemize}
                \item
                If $W[0] \in D_F(\calA)$ then it formally follows that $W \in \calA_F$.
                
                \item
                Assume that $W \in \calA_F$.
                Choose a surjection $Z \to W$ from an object $Z$ that is a direct sum of objects in the essential image of $F$.
                Then the kernel of $Z \to W$ is again contained in $\calA_F$.
                Thus we can similarly choose a surjection $Y \to \ker(Z \to W)$.
                Then we have $W = \coker(Y \to Z)$ as desired.
                
                \item
                Now assume that $W \cong \coker(Y \to Z)$ where $Y$ and $Z$ are direct sums of objects in the essential image of $F$.
                Then we have an exact sequence
                \[
                0 \to K \to Y \to Z \to W \to 0
                \]
                and by the key property and the first claim we have $K[0], Y[0], Z[0] \in D_F(\calA)$.
                It thus follows that also $W[0] \in \calA_F$. \qedhere
            \end{itemize}
        \end{proof}
    \end{claim}
    
    Having established these claims we now make the following observations:
    
    \begin{itemize}
        \item
        We show that $\calA_F$ is stable under cokernels.
        Let $Y \to Z$ be a map in $\calA_F$.
        Then we have the exact sequence
        \[
        0 \to K \to Y \to Z \to Q \to 0
        \]
        and the first three terms are contained in $\calA_F$.
        Thus also the last term is in $\calA_F$ (using the second claim).
        
        \item
        From the above characterisation of objects of $\calA_F$ as cokernels of maps between direct sums of objects in the essential image of $F$ it is clear that $\calA_F \subseteq \calA$ is stable under arbitrary direct sums.
        Thus we see that $\calA_F \subseteq \calA$ is stable under all colimits.
        
        \item
        It is also clear from the second claim that $\calA_F \subseteq \calA$ is stable under extensions.
        
        \item
        We have already noted earlier that the $F(X)$ for $X \in \calA_0$ are projective generators of $\calA_F$.
        Using that the definition of $\calA_F$ and that the objects of $\calA_0$ are projective in $\calA$ we see that the objects $F(X)$ are compact in $\calA_F$.
        
        \item
        Finally, to see that the inclusion $\calA_F \subseteq \calA$ has a left adjoint, it suffices to show that every object in $\calA$ has a left adjoint object.
        So let $Y \in \calA$.
        Then we can write $Y$ as the cokernel of a map $\bigoplus_{i \in I} X'_i \to \bigoplus_{j \in J} X_j$ for some objects $X'_i, X_j \in \calA_0$.
        Define $L(Y)$ to be the cokernel of the induced map $\bigoplus_{i \in I} F(X'_i) \to \bigoplus_{j \in J} F(X_j)$ (as before we use that the objects $X'_i$ and $X_j$ are compact).
        Then, for $Z \in \calA_F$, we have
        \begin{align*}
            \Hom(Y, Z) &\cong \ker \round[\Bigg]{ \prod_{j \in J} \Hom(X_j, Z) \to \prod_{i \in I} \Hom(X'_i, Z) } \\
            &\cong \ker \round[\Bigg]{ \prod_{j \in J} \Hom(F(X_j), Z) \to \prod_{i \in I} \Hom(F(X'_i), Z) } \\
            &\cong \Hom(L(Y), Z).
        \end{align*}
        It is clear that $L$ is the unique colimit-preserving extension of $F$.
    \end{itemize}
    This finishes the proof of the first half of \Cref{lem:technical} and the talk.
\end{document}
